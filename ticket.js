$(document).ready(function () {
    $('.btn').click(function () {
        $("#result").html("");
        let id=$( "#ticketSelect" ).val();
        $.ajax("ticket.php",
            {
                type: "POST",
                data: 'id=' + id,
                success: function (data, textStatus, jqXHR) {
                    console.log(jqXHR.status);
                    $("#result").html(data);
                },
                error: function (xhr,status,error) {
                    console.log(xhr.status);
                }
                
            });
    });
});