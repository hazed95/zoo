<code><!DOCTYPE html>
<html>      
    <head>
        <meta charset="utf-8">
        <link rel='stylesheet' type='text/css' href='style.css' media='all'>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="function.js"></script>
         
        <title>Home</title>
    </head>

    <body>
        <header>
            <div>
                <img src="images/logo/Zootickoon.png" alt="logo" class="logo">
            </div><br>
            <ul class="menu">
                <li><a href="index.php" class="active" >Home</a></li>
                <li><a href="/wordpress/accommodation/">Accomodation</a></li>
                <li><a href="/wordpress/products/">Products</a></li>
                <li><a href="/wordpress/activities/">Activities</a></li>
                <?php 
                session_start();
                if(isset($_SESSION['email']))  { 
                 echo '<li><a href="formTicket.php">Ticket</a></li>';
                 echo '<li><a href="deco.php">Deconnection</a></li>';
                  } 
                  else { 
                 echo '<li><a href="authentification.php">Account</a></li>';
                    }
                ?>
                
              </ul>
        </header><hr>

        <h1 class="centre">Zoo sector:</h1>
        <div class="encadre">
            <h2>Sector 1: Polar Bear</h2>
            <div class="itemPres">
                <img src="images/sector/bear.jpg" alt="Bear-Sector" class="imgZoom">
                <div class="text">
                    In this 150m² area, come and discover our majestic polar bears. If you are lucky, you will be able to see the whole 
                    little family with our magnificent little bear cub. We currently have 3 bears in this enclosure and the maximum 
                    capacity is 4 bears.
                </div>
            </div>
        </div> <br>

        <div class="encadre">
            <h2>Sector 2: Pinguin</h2>
            <div class="itemPres">
                <img src="images/sector/pinguin.jpg" alt="Pinguin-Sector" class="imgZoom">
                <div class="text">
                    In this part of the zoo between the ice floe and the ocean, we can see the presence of about fifty penguins.
                    These penguins have a very large space of about 200 square meters.
                    Unlike penguins, penguins know how to fly and can sometimes show off their flair skills. Pay attention to the show.
                </div>
            </div>
        </div> <br>

        <div class="encadre">
            <h2>Sector 3: White Wolf</h2>
            <div class="itemPres">
                <img src="images/sector/wolf.jpg" alt="Wolf-Sector" class="imgZoom">
                <div class="text">
                    On the ice plain, behind these large and large barriers, you can observe 5 white wolves including a 5-month-old baby white wolf.
                    In this small space of about fifty meters / square, they generally walk in packs, but you will be attracted 
                    by the alpha male who walks very regularly alone.
                </div>
            </div>
        </div> <br>

        <div class="encadre">
            <h2>Sector 4: Seal</h2>
            <div class="itemPres">
                <img src="images/sector/seal.png" alt="Wolf-Sector" class="imgZoom">
                <div class="text">
                    Sea lions, marine mammals, lying on flat rocks, can sometimes tan in the sun.
                    You can sometimes see them jumping in the water to catch fish. In this area of the park, there are about twenty of 
                    them on about 75 square meters.
                </div>
            </div>
        </div> <br><br>

        <?php
           if (intval(date('H'))>=6 && intval(date('H'))<=12)
              echo '<img src="images/sector/zèbre.jpg"/>';
          else if (intval(date('H'))>=13 && intval(date('H'))<=17)
              echo '<img src="images/sector/girafe.jpg"/>';
          else if (intval(date('H'))>=18)
              echo '<img src="images/sector/panda.jpg"/>' 
              ?>
    
        <footer >
        <ul class="menu">
                <li><a href="legalnotice.html" >Legal Notice</a></li>
              </ul>
        <button class="btn btn-success" type="submit"  id="btn" name="Ajax">Ajax</button>
        <div id="result">
        </div>
        </footer>
    </body>

</html></code>
